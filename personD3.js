$(function(){ 
    //2、获取当前屏幕的宽高当作导向图的宽高作用域
    var width = document.getElementById('main').offsetWidth;
    var height = document.documentElement.clientHeight-171;
    var aid = decodeURI(getUrl('aid'));
    $.ajax({
        type : "get",
        url: "请求的json塑聚('person.json')格式",
        cache:false,//不缓存数据
        data:{
           'aid':aid,
           'layer':20
        },
        dataType : "json",//数据类型为json
//            jsonp: "callback",//服务端用于接收callback调用的function名的参数
        success : function(root){
            console.log(root)
            var list = root.data;
            var links = list.rels;
            if(links){
                var counts = (list.nodes).length;
                var countNum = document.getElementById("count").innerHTML=counts;
            var nodes = {};
            links.forEach(function(link) {
                link.source = nodes[link.source] || (nodes[link.source] = {name: link.sourcename});                
                link.target = nodes[link.target] || (nodes[link.target] = {name: link.targetname});
            });
            console.log(nodes)
            //1、创建画布
            var linkLenth =100;
            var img_w = 60;
            var img_h = 60;
            var force = d3.layout.force()//layout将json格式转化为力学图可用的格式
                .nodes(d3.values(nodes))//设定节点数组
                .links(links)//设定连线数组
                .size([width, height])//作用域的大小
                .linkDistance(linkLenth)
                .charge(-500)//顶点的电荷数。该参数决定是排斥还是吸引，数值越小越互相排斥
                .on("start",function(d){
                    d.fixed = true;
                })
                .on("tick", tick)//指时间间隔，隔一段时间刷新一次画面
                .start();//开始转换
                //2、设置svg作用域
            var svg = d3.select("#main").append("svg")
                .attr("width",width)
                .attr("height",height)
          
            //3、设置箭头
            var marker=
                svg.append("marker")
                //.attr("id", function(d) { return d; })
                .attr("id", "resolved")
                //.attr("markerUnits","strokeWidth")//设置为strokeWidth箭头会随着线的粗细发生变化
                .attr("markerUnits","userSpaceOnUse")
                .attr("viewBox", "0 -5 10 10")//坐标系的区域
                .attr("refX",42)//箭头在线上的位置坐标
                .attr("refY", -1)
                .attr("markerWidth", 6)//箭头的大小
                .attr("markerHeight", 6)
                .attr("orient", "auto")//绘制方向，可设定为：auto（自动确认方向）和 角度值//
                .attr("stroke-width",2)//箭头宽度
                .append("path")
                .attr("d", "M0,-5L10,0L0,5")//箭头的路径
                .attr('fill','#969696');//箭头颜色
            //4、设置连接线
            var edges_line = svg.selectAll(".link")
                .data(force.links())
                .enter()
                .append("path")
                .attr({
                      'd': function(d) {
                            return 'M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y
                      },
                      'class':'link',
                      'id':function(d,i) {
                            return 'link'+i;
                      }
                    })
                .style("stroke",function(d){
                    //根据状态设定指向的颜色
                     var lineColor;
                     lineColor = "#A254A2"
                     return lineColor;
                 })
                .style("pointer-events", "none")
                .style("stroke-width",0.5)//线条粗细
                .attr("marker-end", "url(#resolved)" );//根据箭头标记的id号标记箭头
            //5.设定线上字体
            var edges_text = svg.append("g").selectAll(".edgelabel")
                .data(force.links())
                .enter()
                .append("text")
                .style("pointer-events", "none")
                .attr({  'class':'edgelabel',
                    'id':function(d,i){return 'link'+i;},
                    'dx':40,
                    'dy':0,
                    'font-size':8
                    });
            //6、设置线条上的文字,线上区别名，关系为空
            edges_text.append('textPath')
                .attr('xlink:href',function(d,i) {return '#link'+i})
                .style("pointer-events", "none")
                .attr("class","linetext")
                .text(function(d){
                    var res = d.relation;
                    var patt1=new RegExp("SPREAD");
                    if(patt1.test(res)){
                         return '';
                    }else{
                         return  '';
                    }
                });
            //7、置圆圈中的背景颜色,增加背景图片及背景颜色
            var circle = svg.append("g").selectAll("circle")
                .data(force.nodes())//表示使用force.nodes数据
                .enter()
                .append("circle")
                .style("fill",function(d,i){
                    var color;
                    if(d.index == 0){
                       color="#FA50FA";
                    }else{
                        var defs = svg.append("defs").attr("id", "imgdefs")
                        var catpattern = defs.append("pattern")
                            .attr("id", "catpattern" + i)
                            .attr("height", 1)
                            .attr("width", 1)
                        catpattern.append("image")
                            .attr("x", - (img_w / 2 - 20))
                            .attr("y", - (img_h / 2 - 20))
                            .attr("width", img_w)
                            .attr("height", img_h)
                            .attr("xlink:href", 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3993996670,3975191412&fm=117&gp=0.jpg')
                        color =  "url(#catpattern" + i + ")";
                    }      
                    return color;
                })
                .style('stroke',function(node){ 
                    var color;//圆圈线条的颜色
                    var link=links[node.index];
                        color="#A254A2";
                    return color;
                })
                .attr("r", 20)//设置圆圈半径
                //当点击后一个节点时，根据当前被点击的节点和当前nodeJson数据中的name进行匹配,相同的进行线加粗
                .on("click",function(node){
                   //点击节点事件
                })
                .call(force.drag);//将当前选中的元素传到drag函数中，使顶点可以被拖动
            //8、圆圈提示文字
            circle.append("svg:title")  
                .text(function(node) { 
                    var link=links[node.index];
                    return "双击可查看详情"
                 }) 
            //9、设置圆圈内容的字体样式及换行逻辑
            var text = svg.append("g").selectAll("text")
                .data(force.nodes())
                //返回缺失元素的占位对象（placeholder），指向绑定的数据中比选定元素集多出的一部分元素。
                .enter()
                .append("text")
                .attr("dy", ".35em")  
                .attr("text-anchor", "middle")//在圆圈中加上数据  
                .style('fill',function(node){
                    var color;//文字颜色
                    var link=links[node.index];
                    color="A254A2";
                    return color;
                }).attr('x',function(d){
                    var re_en = /[a-zA-Z]+/g;
                    //如果是全英文，不换行
                    if(d.name.match(re_en)){
                         d3.select(this).append('tspan')
                         .attr('x',0)
                         .attr('y',2)
                         .text(function(){return d.name;});
                    }
                    //如果小于四个字符，不换行
                    else if(d.name.length<=4){
                         d3.select(this).append('tspan')
                        .attr('x',0)
                        .attr('y',2)
                        .text(function(){return d.name;});
                    }else{
                        var top=d.name.substring(0,4);
                        var bot=d.name.substring(4,d.name.length);

                        d3.select(this).text(function(){return '';});

                        d3.select(this).append('tspan')
                            .attr('x',0)
                            .attr('y',-7)
                            .text(function(){return top;});

                        d3.select(this).append('tspan')
                            .attr('x',0)
                            .attr('y',10)
                            .text(function(){return bot;});
                    }
                });
                var text_dx = -20;
                var text_dy = 20;
            //进行拖拽的话固定顶点位置
            var drag = force.drag()  
                .on("dragstart",function(d,i){  
                    d.fixed = true;    //拖拽开始后设定被拖拽对象为固定  
                   
                })  
            //end执行tick，动态更新项目
            function tick(d) {
                //限制边框
                force.nodes().forEach(function(d,i){
                    d.x = d.x - img_w/2 < 0 ? img_w/2 : d.x ;
                    d.x = d.x + img_w/2 > width ? width - img_w/2 : d.x ;
                    d.y = d.y - img_h/2 < 0 ? img_h/2 : d.y ;
                    d.y = d.y + img_h/2 + text_dy > height ? height - img_h/2 - text_dy : d.y ;
                });
                 //更新连接线的位置
                edges_line.attr("x1",function(d){ return d.source.x; });
                edges_line.attr("y1",function(d){  return d.source.y; });
                edges_line.attr("x2",function(d){ return d.target.x; });
                edges_line.attr("y2",function(d){ return d.target.y; });
                circle.attr("transform", transform1);//圆圈
                text.attr("transform", transform2);//顶点文字
              //获取节点中的x y坐标
                edges_line.attr('d', function(d) { 
                    var path='M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y;
                    return path;
                });  
                edges_text.attr('transform',function(d,i){
                      if (d.target.x<d.source.x){
                          bbox = this.getBBox();
                          rx = bbox.x+bbox.width/2;
                          ry = bbox.y+bbox.height/2;
                          return 'rotate(180 '+rx+' '+ry+')';
                      }
                      else {
                          return 'rotate(0)';
                      }
                 });
              }

              //设置连接线的坐标,使用椭圆弧路径段双向编码
              function linkArc(d) {
                return 'M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y
              }
              //设置圆圈和文字的坐标
              function transform1(d) {
                return "translate(" + d.x + "," + d.y + ")";
              }
              function transform2(d) {
                    d.x.fixed = true;
                    return "translate(" + (d.x) + "," + d.y + ")";
              }  
          }else{
               var message = document.getElementById("main").innerHTML='没有相关数据';
          }
        },
    });
     function getUrl(para){
        var paraArr = location.search.substring(1).split('&');
        for(var i = 0;i < paraArr.length;i++){
            if(para == paraArr[i].split('=')[0]){
                return paraArr[i].split('=')[1];
            }
        }
        return '';
    }
    function dragmove(d) {	
        d3.select(this)
          .attr("cx", d.cx = d3.event.x )
          .attr("cy", d.cy = d3.event.y );
    }
   
    
  
})